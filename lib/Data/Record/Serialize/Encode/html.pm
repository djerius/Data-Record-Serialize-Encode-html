package Data::Record::Serialize::Encode::html;

# ABSTRACT: encode a record as html

use v5.10;

use Moo::Role;

our $VERSION = '0.03';

use warnings::register;

use namespace::clean;

has _html => (
    is       => 'lazy',
    init_arg => undef,
    builder  => sub {
        require HTML::Tiny;
        HTML::Tiny->new( mode => 'html' );
    },
);

has _closed => (
    is       => 'rwp',
    init_arg => undef,
    default  => 1,
);

=attr table_class

=attr thead_class

=attr tbody_class

=attr tr_class

=attr th_class

=attr td_class

See L</CONSTRUCTOR ARGUMENTS>.

Optional.

=for Pod::Coverage
has_table_class
has_thead_class
has_tbody_class
has_tr_class
has_th_class
has_td_class

=cut

has [ 'table_class', 'thead_class', 'tbody_class', 'td_class', 'tr_class', 'th_class' ] => (
    is        => 'ro',
    predicate => 1,
);

has _class => (
    is        => 'lazy',
    init_args => undef,
    builder   => sub {
        my $self = shift;
        {
            table => $self->has_table_class ? { class => $self->table_class } : undef,
            thead => $self->has_thead_class ? { class => $self->thead_class } : undef,
            th    => $self->has_th_class    ? { class => $self->th_class }    : undef,
            td    => $self->has_td_class    ? { class => $self->td_class }    : undef,
            tr    => $self->has_tr_class    ? { class => $self->tr_class }    : undef,
        };
    },
);

sub _needs_eol { 0 }

sub encode {
    my $self = shift;
    $self->_html->tr( [
            $self->_html->td(
                $self->_class->{td} // (),
                map { $_ // q{} } @{ $_[0] }{ @{ $self->output_fields } } ) ] );
}

sub setup {
    my $self = shift;
    my $html = $self->_html;
    $self->say( $html->open( 'table', $self->_class->{table} // () ) );

    $self->say(
        $html->thead( [
                $html->tr(
                    $self->_class->{'tr'} // (),
                    [ $html->th( $self->_class->{th} // (), @{ $self->output_fields } ) ],
                ),
            ],
        ),
    );
    $self->say( $html->open( 'tbody', $self->_class->{tbody} // () ) );
    $self->_set__closed( 0 );
}

sub finalize {
    my $self = shift;
    return if $self->_closed;
    $self->say( $self->_html->close( 'tbody' ) );
    $self->say( $self->_html->close( 'table' ) );
    $self->_set__closed( 1 );
}


sub DEMOLISH {
    my $self = shift;

    warnings::warnif( 'Data::Record::Serialize::Encode::html',
        __PACKAGE__ . ': html table is not closed' )
      unless $self->_closed;
}

with 'Data::Record::Serialize::Role::Encode';

1;

# COPYRIGHT

__END__

=pod

=for Pod::Coverage
encode
setup
finalize
DEMOLISH

=head1 SYNOPSIS

    use Data::Record::Serialize;
    my $s = Data::Record::Serialize->new( encode => 'html', ... );
    $s->send( \%record );

=head1 DESCRIPTION

B<Data::Record::Serialize::Encode::html> encodes a record as HTML.

It performs the L<Data::Record::Serialize::Role::Encode> role.

You cannot construct this directly. You must use L<Data::Record::Serialize/new>.

=head1 CONSTRUCTOR OPTIONS

=head3 table_class

=head3 thead_class

=head3 tbody_class

=head3 tr_class

=head3 th_class

=head3 td_class

The CSS class associated with the given element.  All are optional.

=head1 SEE ALSO

=cut
