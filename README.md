# NAME

Data::Record::Serialize::Encode::html - encode a record as html

# VERSION

version 0.03

# SYNOPSIS

    use Data::Record::Serialize;
    my $s = Data::Record::Serialize->new( encode => 'html', ... );
    $s->send( \%record );

# DESCRIPTION

**Data::Record::Serialize::Encode::html** encodes a record as HTML.

It performs the [Data::Record::Serialize::Role::Encode](https://metacpan.org/pod/Data%3A%3ARecord%3A%3ASerialize%3A%3ARole%3A%3AEncode) role.

You cannot construct this directly. You must use ["new" in Data::Record::Serialize](https://metacpan.org/pod/Data%3A%3ARecord%3A%3ASerialize#new).

# OBJECT ATTRIBUTES

## table\_class

## thead\_class

## tbody\_class

## tr\_class

## th\_class

## td\_class

See ["CONSTRUCTOR ARGUMENTS"](#constructor-arguments).

Optional.

# CONSTRUCTOR OPTIONS

### table\_class

### thead\_class

### tbody\_class

### tr\_class

### th\_class

### td\_class

The CSS class associated with the given element.  All are optional.

# SUPPORT

## Bugs

Please report any bugs or feature requests to bug-data-record-serialize-encode-html@rt.cpan.org  or through the web interface at: [https://rt.cpan.org/Public/Dist/Display.html?Name=Data-Record-Serialize-Encode-html](https://rt.cpan.org/Public/Dist/Display.html?Name=Data-Record-Serialize-Encode-html)

## Source

Source is available at

    https://gitlab.com/djerius/data-record-serialize-encode-html

and may be cloned from

    https://gitlab.com/djerius/data-record-serialize-encode-html.git

# AUTHOR

Diab Jerius <djerius@cpan.org>

# COPYRIGHT AND LICENSE

This software is Copyright (c) 2024 by Smithsonian Astrophysical Observatory.

This is free software, licensed under:

    The GNU General Public License, Version 3, June 2007
